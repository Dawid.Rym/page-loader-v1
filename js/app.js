    //global variables
    let loader = document.querySelector('.loader');
    let mainEl = document.querySelector('.main');
    mainEl.style.display = "none";

    //Loading event
    setTimeout(()=>{
        mainEl.style.display = "block";
        loader.classList.remove('loader')
    },4000);
